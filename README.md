# GT SAT INFILE API

[![pipeline status](https://gitlab.com/HomebrewSoft/l10n_gt/gt_sat_infile_api/badges/main/pipeline.svg)](https://gitlab.com/HomebrewSoft/l10n_gt/gt_sat_infile_api/-/commits/main)

[![coverage report](https://gitlab.com/HomebrewSoft/l10n_gt/gt_sat_infile_api/badges/main/coverage.svg)](https://gitlab.com/HomebrewSoft/l10n_gt/gt_sat_infile_api/-/commits/main)

- API comunicación INFILE (analogo al PAC)
  - repo
    - gt_sat_infile_api
      - Login Handler
      - Connector
      - Parser HTTP a Factura
      - Excepciones
    - tests
